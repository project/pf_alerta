<?php

namespace Drupal\pf_alerta\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\push_framework\Form\Settings as FrameworkSettings;

/**
 * Configure Push Framework Alerta settings.
 */
class Settings extends FrameworkSettings {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'pf_alerta_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['pf_alerta.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['alerta_details'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="active"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['alerta_details']['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Url'),
      '#default_value' => $this->pluginConfig->get('url'),
    ];
    $form['alerta_details']['apikey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $this->pluginConfig->get('apikey'),
    ];
    $form['alerta_details']['environment'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Environment'),
      '#default_value' => $this->pluginConfig->get('environment'),
    ];
    $form['alerta_details']['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#default_value' => $this->pluginConfig->get('host'),
    ];
    $form['alerta_details']['pid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GitLab Project ID'),
      '#default_value' => $this->pluginConfig->get('pid'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->pluginConfig
      ->set('url', $form_state->getValue('url'))
      ->set('apikey', $form_state->getValue('apikey'))
      ->set('environment', $form_state->getValue('environment'))
      ->set('host', $form_state->getValue('host'))
      ->set('pid', $form_state->getValue('pid'));
    parent::submitForm($form, $form_state);
  }

}
