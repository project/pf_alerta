<?php

namespace Drupal\pf_alerta\Plugin\DanseRecipientSelection;

use Drupal\push_framework\Plugin\DanseRecipientSelection\DirectPush;

/**
 * Plugin implementation of DANSE.
 *
 * @DanseRecipientSelection(
 *   id = "alerta",
 *   label = @Translation("Alerta"),
 *   description = @Translation("Marks notifications to be sent to alerta only.")
 * )
 */
class Alerta extends DirectPush {

  /**
   * {@inheritdoc}
   */
  public function directPushChannelId(): string {
    return 'alerta';
  }

}
