<?php

namespace Drupal\pf_alerta\Plugin\PushFrameworkChannel;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\danse\Entity\EventInterface;
use Drupal\danse_log\Payload;
use Drupal\push_framework\ChannelBase;
use Drupal\user\UserInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the push framework channel.
 *
 * @ChannelPlugin(
 *   id = "alerta",
 *   label = @Translation("Alerta"),
 *   description = @Translation("Provides the Alerta channel plugin.")
 * )
 */
class Alerta extends ChannelBase {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->time = $container->get('datetime.time');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigName(): string {
    return 'pf_alerta.settings';
  }

  /**
   * {@inheritdoc}
   */
  public function applicable(UserInterface $user): bool {
    // This channel sends directly, so it never applies to other notifications.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function send(UserInterface $user, ContentEntityInterface $entity, array $content, int $attempt): string {
    $output = array_shift($content);
    $body = [
      'origin' => $this->pluginConfig->get('host'),
      'timeout' => 86400,
      'environment' => $this->pluginConfig->get('environment'),
      'resource' => 'undefined-' . $this->time->getRequestTime(),
      'event' => 'Exception',
      'tags' => ['environment:' . $this->pluginConfig->get('environment')],
      'group' => 'Application',
      'severity' => 'critical',
      'service' => ['www.example.com'],
      'text' => strip_tags($output['subject'] ?? '') . ': ' . strip_tags($output['body'] ?? ''),
      'value' => '',
      'rawData' => '',
    ];
    if (!empty($this->pluginConfig->get('pid'))) {
      $body['attributes'] = [
        'gitlab_project_id' => $this->pluginConfig->get('pid'),
      ];
    }

    /* @noinspection ClassConstantCanBeUsedInspection */
    if (class_exists('\Drupal\danse_log\Payload') &&
      $entity instanceof EventInterface &&
      ($payload = $entity->getPayload()) &&
      $payload instanceof Payload) {
      $context = $payload->getContext();
      $text = new FormattableMarkup($payload->getMessage(), $context['placeholders']);
      $subject = strip_tags((string) $text);
      $host = parse_url($context['request_uri'], PHP_URL_HOST);
      $event = mb_substr($context['channel'], 0, 64);

      $body['resource'] = hash('md5', $host . $subject);
      $body['event'] = $event;
      $body['service'] = [$host];
      $body['text'] = explode("\n", str_replace('\\', '/', $subject))[0];
      $body['rawData'] = [
        'uid' => $context['uid'],
        'type' => $event,
        'severity' => $payload->getLevel(),
        'link' => $context['link'],
        'location' => $context['request_uri'],
        'referer' => $context['referer'],
        'hostname' => mb_substr($context['ip'], 0, 128),
        'timestamp' => $context['timestamp'],
        'message' => $subject,
        'backtrace' => $context['backtrace'],
      ];
    }

    $options = [
      'headers' => [
        'Authorization' => 'Key ' . $this->pluginConfig->get('apikey'),
        'Content-Type' => 'application/json',
      ],
    ];
    try {
      $options['body'] = json_encode($body, JSON_THROW_ON_ERROR);
    }
    catch (\JsonException $e) {
      // We can't log this exception because that would lead to an infinite loop
      // with the logging system.
      $options['body'] = '{}';
    }

    $client = new Client();
    try {
      $response = $client->request('POST', $this->pluginConfig->get('url') . '/alert', $options);
      $statusCode = $response->getStatusCode();
      if ($statusCode === 201) {
        return self::RESULT_STATUS_SUCCESS;
      }
    }
    catch (GuzzleException $e) {
      // @todo Log this exception.
    }
    return self::RESULT_STATUS_FAILED;
  }

}
